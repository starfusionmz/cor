package cor.types.compound;

import cor.Value;
import java.lang.String;
import java.util.Map;

public interface CorMap extends Value, Map<String, Value> {
  CorMap getMapOrEmpty(String key);

  CorList getListOrEmpty(String key);
}

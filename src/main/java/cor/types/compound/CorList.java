package cor.types.compound;

import cor.Value;
import java.util.List;

public interface CorList extends Value, List<Value> {
  CorMap getMapOrEmpty(int index);

  CorList getListOrEmpty(int index);
}

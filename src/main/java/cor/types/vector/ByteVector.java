package cor.types.vector;

import cor.Value;

public interface ByteVector extends Value {
  byte get(long index);

  long size();
}

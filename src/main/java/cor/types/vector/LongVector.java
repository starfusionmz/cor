package cor.types.vector;

import cor.Value;

public interface LongVector extends Value {
  long get(long index);

  long size();
}

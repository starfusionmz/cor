package cor.types.vector;

import cor.Value;
import java.lang.String;

public interface StringVector extends Value {
  String get(long index);

  long size();
}

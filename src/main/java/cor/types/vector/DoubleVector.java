package cor.types.vector;

import cor.Value;

public interface DoubleVector extends Value {
  double get(long index);

  long size();
}

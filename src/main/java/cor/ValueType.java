package cor;

import static cor.TypeCategory.Compound;
import static cor.TypeCategory.Scalar;
import static cor.TypeCategory.Vector;

public enum ValueType {
	String(Scalar,String.class), Long(Scalar,long.class), Double(Scalar,double.class), Byte(Scalar,byte.class),

	Map(Compound), List(Compound),

	StringVector(Vector, String.class), 
	LongVector(Vector, long.class), 
	DoubleVector(Vector, double.class),
	ByteVector(Vector, byte.class);
	
	public static final String PACKAGE_NAME = "cor.types";
	
	public TypeCategory category;
	public Class<?> elementType;
	
	ValueType(TypeCategory category,Class<?> elementType) {
		this.category = category;
		this.elementType = elementType;
	}
	
	ValueType(TypeCategory category) {
		this.category = category;
		this.elementType =null;
	}
	
	public String getPackage(){
		return PACKAGE_NAME+"."+category.name().toLowerCase();
	}
	
	public String getClassName() {
		if(category==Vector) {
			return name();			
		}
		return "Cor"+name();
	}
	
}

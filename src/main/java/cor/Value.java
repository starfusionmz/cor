package cor;

public interface Value extends Convertible{
	ValueMetadata getMetadata();

	String visibility();

	ValueType type();
}

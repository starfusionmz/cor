package cor.generator;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.lang.model.element.Modifier;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeSpec.Builder;

import cor.TypeCategory;
import cor.Value;
import cor.ValueType;

public class Generator {
	public Generator() throws IOException {
	}

	public void generate() throws IOException {
		Set<ValueSpec> types = generateTypes();
		Set<ConverterSpec> converters = generateConverters(types);
		generateConverterInterface(converters);
	}

	public Set<ValueSpec> generateTypes() {
		Set<ValueSpec> typeSpecs = new LinkedHashSet<>();
		for (ValueType type : ValueType.values()) {
			switch (type.category) {
			case Vector:
				typeSpecs.add(generateVector(type));
				break;
			case Compound:
				if (type == ValueType.Map) {
					typeSpecs.add(generateMap(type));
				} else if (type == ValueType.List) {
					typeSpecs.add(generateList(type));
				}
				break;
			default:
				TypeSpec spec = TypeSpec.interfaceBuilder(type.getClassName()).addModifiers(Modifier.PUBLIC)
						.addSuperinterface(Value.class).build();
				typeSpecs.add(ValueSpec.create(spec, type));
				break;
			}

		}
		return typeSpecs;
	}

	private ValueSpec generateMap(ValueType type) {
		ValueType list = ValueType.List;
		ClassName mapType = ClassName.get(type.getPackage(), type.getClassName());
		ClassName listType = ClassName.get(list.getPackage(), list.getClassName());

		MethodSpec getMapOrEmpty = MethodSpec.methodBuilder("getMapOrEmpty")
				.addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT).returns(mapType).addParameter(String.class, "key")
				.build();

		MethodSpec getListOrEmpty = MethodSpec.methodBuilder("getListOrEmpty")
				.addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT).returns(listType).addParameter(String.class, "key")
				.build();

		ParameterizedTypeName mapInterface = ParameterizedTypeName.get(Map.class, String.class, Value.class);

		TypeSpec spec = TypeSpec.interfaceBuilder(mapType).addModifiers(Modifier.PUBLIC).addSuperinterface(Value.class)
				.addSuperinterface(mapInterface).addMethod(getMapOrEmpty).addMethod(getListOrEmpty).build();

		return ValueSpec.create(spec, type);
	}

	private ValueSpec generateList(ValueType type) {
		ValueType map = ValueType.Map;
		ClassName mapType = ClassName.get(map.getPackage(), map.getClassName());
		ClassName listType = ClassName.get(type.getPackage(), type.getClassName());
		MethodSpec getMapOrEmpty = MethodSpec.methodBuilder("getMapOrEmpty")
				.addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT).returns(mapType).addParameter(TypeName.INT, "index")
				.build();

		MethodSpec getListOrEmpty = MethodSpec.methodBuilder("getListOrEmpty")
				.addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT).returns(listType).addParameter(TypeName.INT, "index")
				.build();
		ParameterizedTypeName javaListType = ParameterizedTypeName.get(List.class, Value.class);
		
		TypeSpec spec = TypeSpec.interfaceBuilder(listType).addModifiers(Modifier.PUBLIC)
				.addSuperinterface(Value.class).addSuperinterface(javaListType).addMethod(getMapOrEmpty)
				.addMethod(getListOrEmpty).build();
		return ValueSpec.create(spec, type);
	}

	private ValueSpec generateVector(ValueType type) {
		MethodSpec getByIndex = MethodSpec.methodBuilder("get").addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
				.returns(type.elementType).addParameter(TypeName.LONG, "index").build();
		MethodSpec sizeMethod = MethodSpec.methodBuilder("size").addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
				.returns(TypeName.LONG).build();
		TypeSpec spec = TypeSpec.interfaceBuilder(type.getClassName()).addModifiers(Modifier.PUBLIC)
				.addSuperinterface(Value.class).addMethod(getByIndex).addMethod(sizeMethod).build();
		return ValueSpec.create(spec, type);
	}

	public Set<ConverterSpec> generateConverters(Set<ValueSpec> types) {
		Set<ConverterSpec> converterSpecs = new LinkedHashSet<ConverterSpec>();
		for (ValueSpec toType : types) {
			String className = String.format("%sConverter", toType.valueType.name());
			
				
			TypeName returnType = generateConverterReturnType(toType.valueType);
			
			Builder builder = TypeSpec.interfaceBuilder(className).addModifiers(Modifier.PUBLIC);
			
			for (ValueSpec paramSpec : types) {
				if (paramSpec == toType) {
					continue;
				}
				ClassName paramType = ClassName.get(paramSpec.getPackageName(),
						paramSpec.getValueType().getClassName());
				MethodSpec method = MethodSpec.methodBuilder("convert").addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
						.returns(returnType).addParameter(paramType, "from").build();
				builder.addMethod(method);
			}
			converterSpecs.add(ConverterSpec.create(builder.build(), toType.getValueType()));
		}
		return converterSpecs;
	}
	
	TypeName generateConverterReturnType(ValueType type){
		if(type.category==TypeCategory.Scalar) {
			return TypeName.get(type.elementType);
		}else {
			return ClassName.get(type.getPackage(),type.getClassName());
		}
	}

	public TypeSpec generateConverterInterface(Set<ConverterSpec> converters) {
		Builder converterBuilder = TypeSpec.interfaceBuilder("Convertible");
		for (ConverterSpec converterSpec : converters) {
			TypeName converterTypeName = ClassName.get(converterSpec.getPackageName(),
					converterSpec.getConverterSpec().name);
			
			TypeName returnType = converterSpec.getReturnType();
			
			MethodSpec method = MethodSpec.methodBuilder("as").addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
					.addParameter(converterTypeName, "form").returns(returnType).build();
			converterBuilder.addMethod(method);
		}
		return converterBuilder.build();
	}

	public static void main(String[] args) throws IOException {
		Generator generator = new Generator();
		generator.generate();
	}
}

package cor.generator;

import com.squareup.javapoet.TypeSpec;

import cor.ValueType;

public class ValueSpec {
	final TypeSpec valueSpec;

	final ValueType valueType;
	
	public static ValueSpec create(TypeSpec spec,ValueType type){
		return new ValueSpec(spec, type);
	}
	
	public ValueSpec(TypeSpec valueSpec, ValueType valueType) {
		this.valueSpec = valueSpec;
		this.valueType = valueType;
	}

	public TypeSpec getValueSpec() {
		return valueSpec;
	}
	
	public String getPackageName() {
		return ValueType.PACKAGE_NAME+"."+getValueType().category.name().toLowerCase();
	}

	public ValueType getValueType() {
		return valueType;
	}

}

package cor.generator;

import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import cor.ValueType;

public class ConverterSpec {
	public static final String CONVERTER_PACKAGE = "cor.converters";
	
	final TypeSpec converterSpec;

	final ValueType valueType;
	
	public static ConverterSpec create(TypeSpec spec,ValueType type){
		return new ConverterSpec(spec, type);
	}
	
	public ConverterSpec(TypeSpec valueSpec, ValueType valueType) {
		this.converterSpec = valueSpec;
		this.valueType = valueType;
	}

	public TypeSpec getConverterSpec() {
		return converterSpec;
	}
	
	public String getPackageName() {
		return CONVERTER_PACKAGE;
	}

	public ValueType getValueType() {
		return valueType;
	}

	public TypeName getReturnType() {
		return getConverterSpec().methodSpecs.get(0).returnType;
	}

}

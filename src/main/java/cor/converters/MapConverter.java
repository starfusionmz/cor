package cor.converters;

import cor.types.compound.CorList;
import cor.types.compound.CorMap;
import cor.types.scalar.CorByte;
import cor.types.scalar.CorDouble;
import cor.types.scalar.CorLong;
import cor.types.scalar.CorString;
import cor.types.vector.ByteVector;
import cor.types.vector.DoubleVector;
import cor.types.vector.LongVector;
import cor.types.vector.StringVector;

public interface MapConverter {
  CorMap convert(CorString from);

  CorMap convert(CorLong from);

  CorMap convert(CorDouble from);

  CorMap convert(CorByte from);

  CorMap convert(CorList from);

  CorMap convert(StringVector from);

  CorMap convert(LongVector from);

  CorMap convert(DoubleVector from);

  CorMap convert(ByteVector from);
}

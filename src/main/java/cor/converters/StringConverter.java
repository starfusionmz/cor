package cor.converters;

import cor.types.compound.CorList;
import cor.types.compound.CorMap;
import cor.types.scalar.CorByte;
import cor.types.scalar.CorDouble;
import cor.types.scalar.CorLong;
import cor.types.vector.ByteVector;
import cor.types.vector.DoubleVector;
import cor.types.vector.LongVector;
import cor.types.vector.StringVector;
import java.lang.String;

public interface StringConverter {
  String convert(CorLong from);

  String convert(CorDouble from);

  String convert(CorByte from);

  String convert(CorMap from);

  String convert(CorList from);

  String convert(StringVector from);

  String convert(LongVector from);

  String convert(DoubleVector from);

  String convert(ByteVector from);
}

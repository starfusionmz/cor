package cor.converters;

import cor.types.compound.CorList;
import cor.types.compound.CorMap;
import cor.types.scalar.CorByte;
import cor.types.scalar.CorDouble;
import cor.types.scalar.CorString;
import cor.types.vector.ByteVector;
import cor.types.vector.DoubleVector;
import cor.types.vector.LongVector;
import cor.types.vector.StringVector;

public interface LongConverter {
  long convert(CorString from);

  long convert(CorDouble from);

  long convert(CorByte from);

  long convert(CorMap from);

  long convert(CorList from);

  long convert(StringVector from);

  long convert(LongVector from);

  long convert(DoubleVector from);

  long convert(ByteVector from);
}

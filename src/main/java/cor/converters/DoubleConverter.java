package cor.converters;

import cor.types.compound.CorList;
import cor.types.compound.CorMap;
import cor.types.scalar.CorByte;
import cor.types.scalar.CorLong;
import cor.types.scalar.CorString;
import cor.types.vector.ByteVector;
import cor.types.vector.DoubleVector;
import cor.types.vector.LongVector;
import cor.types.vector.StringVector;

public interface DoubleConverter {
  double convert(CorString from);

  double convert(CorLong from);

  double convert(CorByte from);

  double convert(CorMap from);

  double convert(CorList from);

  double convert(StringVector from);

  double convert(LongVector from);

  double convert(DoubleVector from);

  double convert(ByteVector from);
}

package cor.converters;

import cor.types.compound.CorList;
import cor.types.compound.CorMap;
import cor.types.scalar.CorDouble;
import cor.types.scalar.CorLong;
import cor.types.scalar.CorString;
import cor.types.vector.ByteVector;
import cor.types.vector.DoubleVector;
import cor.types.vector.LongVector;
import cor.types.vector.StringVector;

public interface ByteConverter {
  byte convert(CorString from);

  byte convert(CorLong from);

  byte convert(CorDouble from);

  byte convert(CorMap from);

  byte convert(CorList from);

  byte convert(StringVector from);

  byte convert(LongVector from);

  byte convert(DoubleVector from);

  byte convert(ByteVector from);
}

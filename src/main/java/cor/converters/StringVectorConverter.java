package cor.converters;

import cor.types.compound.CorList;
import cor.types.compound.CorMap;
import cor.types.scalar.CorByte;
import cor.types.scalar.CorDouble;
import cor.types.scalar.CorLong;
import cor.types.scalar.CorString;
import cor.types.vector.ByteVector;
import cor.types.vector.DoubleVector;
import cor.types.vector.LongVector;
import cor.types.vector.StringVector;

public interface StringVectorConverter {
  StringVector convert(CorString from);

  StringVector convert(CorLong from);

  StringVector convert(CorDouble from);

  StringVector convert(CorByte from);

  StringVector convert(CorMap from);

  StringVector convert(CorList from);

  StringVector convert(LongVector from);

  StringVector convert(DoubleVector from);

  StringVector convert(ByteVector from);
}

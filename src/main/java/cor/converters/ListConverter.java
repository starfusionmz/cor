package cor.converters;

import cor.types.compound.CorList;
import cor.types.compound.CorMap;
import cor.types.scalar.CorByte;
import cor.types.scalar.CorDouble;
import cor.types.scalar.CorLong;
import cor.types.scalar.CorString;
import cor.types.vector.ByteVector;
import cor.types.vector.DoubleVector;
import cor.types.vector.LongVector;
import cor.types.vector.StringVector;

public interface ListConverter {
  CorList convert(CorString from);

  CorList convert(CorLong from);

  CorList convert(CorDouble from);

  CorList convert(CorByte from);

  CorList convert(CorMap from);

  CorList convert(StringVector from);

  CorList convert(LongVector from);

  CorList convert(DoubleVector from);

  CorList convert(ByteVector from);
}

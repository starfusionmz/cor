package cor.converters;

import cor.types.compound.CorList;
import cor.types.compound.CorMap;
import cor.types.scalar.CorByte;
import cor.types.scalar.CorDouble;
import cor.types.scalar.CorLong;
import cor.types.scalar.CorString;
import cor.types.vector.ByteVector;
import cor.types.vector.DoubleVector;
import cor.types.vector.LongVector;
import cor.types.vector.StringVector;

public interface LongVectorConverter {
  LongVector convert(CorString from);

  LongVector convert(CorLong from);

  LongVector convert(CorDouble from);

  LongVector convert(CorByte from);

  LongVector convert(CorMap from);

  LongVector convert(CorList from);

  LongVector convert(StringVector from);

  LongVector convert(DoubleVector from);

  LongVector convert(ByteVector from);
}

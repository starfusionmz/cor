package cor.converters;

import cor.types.compound.CorList;
import cor.types.compound.CorMap;
import cor.types.scalar.CorByte;
import cor.types.scalar.CorDouble;
import cor.types.scalar.CorLong;
import cor.types.scalar.CorString;
import cor.types.vector.ByteVector;
import cor.types.vector.DoubleVector;
import cor.types.vector.LongVector;
import cor.types.vector.StringVector;

public interface DoubleVectorConverter {
  DoubleVector convert(CorString from);

  DoubleVector convert(CorLong from);

  DoubleVector convert(CorDouble from);

  DoubleVector convert(CorByte from);

  DoubleVector convert(CorMap from);

  DoubleVector convert(CorList from);

  DoubleVector convert(StringVector from);

  DoubleVector convert(LongVector from);

  DoubleVector convert(ByteVector from);
}

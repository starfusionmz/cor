package cor.converters;

import cor.types.compound.CorList;
import cor.types.compound.CorMap;
import cor.types.scalar.CorByte;
import cor.types.scalar.CorDouble;
import cor.types.scalar.CorLong;
import cor.types.scalar.CorString;
import cor.types.vector.ByteVector;
import cor.types.vector.DoubleVector;
import cor.types.vector.LongVector;
import cor.types.vector.StringVector;

public interface ByteVectorConverter {
  ByteVector convert(CorString from);

  ByteVector convert(CorLong from);

  ByteVector convert(CorDouble from);

  ByteVector convert(CorByte from);

  ByteVector convert(CorMap from);

  ByteVector convert(CorList from);

  ByteVector convert(StringVector from);

  ByteVector convert(LongVector from);

  ByteVector convert(DoubleVector from);
}

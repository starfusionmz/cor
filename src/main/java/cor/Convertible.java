package cor;

import cor.converters.ByteConverter;
import cor.converters.ByteVectorConverter;
import cor.converters.DoubleConverter;
import cor.converters.DoubleVectorConverter;
import cor.converters.ListConverter;
import cor.converters.LongConverter;
import cor.converters.LongVectorConverter;
import cor.converters.MapConverter;
import cor.converters.StringConverter;
import cor.converters.StringVectorConverter;
import cor.types.compound.CorList;
import cor.types.compound.CorMap;
import cor.types.vector.ByteVector;
import cor.types.vector.DoubleVector;
import cor.types.vector.LongVector;
import cor.types.vector.StringVector;
import java.lang.String;

interface Convertible {
  String as(StringConverter form);

  long as(LongConverter form);

  double as(DoubleConverter form);

  byte as(ByteConverter form);

  CorMap as(MapConverter form);

  CorList as(ListConverter form);

  StringVector as(StringVectorConverter form);

  LongVector as(LongVectorConverter form);

  DoubleVector as(DoubleVectorConverter form);

  ByteVector as(ByteVectorConverter form);
}

package cor;

public interface ValueMetadata {
	public void put(String key,String value);
	public String get(String key);
}

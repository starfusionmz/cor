package cor;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeSpec;

import cor.generator.ConverterSpec;
import cor.generator.Generator;
import cor.generator.ValueSpec;

public class GeneratorTest {
	@Test
	public void generateTypes() throws IOException {
		Generator generator = new Generator();
		Set<ValueSpec> types = generator.generateTypes();
		System.out.println(types);
	}

	@Test
	public void generateTypeFiles() throws IOException {
		Generator generator = new Generator();
		Set<ValueSpec> types = generator.generateTypes();

		
		Set<JavaFile> files = new HashSet<JavaFile>();
		for (ValueSpec spec : types) {
			files.add(JavaFile.builder(spec.getPackageName(), spec.getValueSpec()).build());
		}
		
		Set<ConverterSpec> converters=generator.generateConverters(types);
		
		for (ConverterSpec spec : converters) {
			files.add(JavaFile.builder(spec.getPackageName(), spec.getConverterSpec()).build());
		}

		TypeSpec converterInterface =generator.generateConverterInterface(converters);
		files.add(JavaFile.builder("cor", converterInterface).build());
		
		Path path = Paths.get("src/main/java/");
		for (JavaFile file : files) {
			file.writeTo(path);
		}
		System.out.println(files);
	}

	@Test
	public void generateConverters() throws IOException {
		Generator generator = new Generator();
		Set<ValueSpec> types = generator.generateTypes();
		Set<ConverterSpec> converters = generator.generateConverters(types);
		System.out.println(converters);
	}

	@Test
	public void testGenerateConverterType() throws IOException {
		Generator generator = new Generator();
		Set<ValueSpec> types = generator.generateTypes();
		Set<ConverterSpec> converters = generator.generateConverters(types);
		TypeSpec cInterface = generator.generateConverterInterface(converters);
		System.out.println(cInterface);
	}
}
